-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bus_stop_locator
CREATE DATABASE IF NOT EXISTS `bus_stop_locator` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bus_stop_locator`;

-- Dumping structure for table bus_stop_locator.bus_details
CREATE TABLE IF NOT EXISTS `bus_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bus_stop_locator.bus_details: ~8 rows (approximately)
DELETE FROM `bus_details`;
/*!40000 ALTER TABLE `bus_details` DISABLE KEYS */;
INSERT INTO `bus_details` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'VRL Bus Services', NULL, NULL),
	(2, 'Sea Bird', NULL, NULL),
	(3, 'Metrozip', NULL, NULL),
	(4, 'City Service', NULL, NULL),
	(5, 'city Service Volvo', NULL, NULL),
	(6, 'SRS', NULL, NULL),
	(7, 'Sarma Transport', NULL, NULL),
	(8, 'Gemini', NULL, NULL);
/*!40000 ALTER TABLE `bus_details` ENABLE KEYS */;

-- Dumping structure for table bus_stop_locator.bus_locations
CREATE TABLE IF NOT EXISTS `bus_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bus_id` int(11) NOT NULL,
  `bus_stop_id` int(11) NOT NULL,
  `arrival_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bus_stop_locator.bus_locations: ~45 rows (approximately)
DELETE FROM `bus_locations`;
/*!40000 ALTER TABLE `bus_locations` DISABLE KEYS */;
INSERT INTO `bus_locations` (`id`, `bus_id`, `bus_stop_id`, `arrival_time`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '10:00:00', NULL, NULL),
	(2, 2, 1, '11:00:00', NULL, NULL),
	(3, 3, 1, '12:00:00', NULL, NULL),
	(4, 4, 1, '13:00:00', NULL, NULL),
	(5, 5, 1, '14:00:00', NULL, NULL),
	(6, 6, 1, '15:00:00', NULL, NULL),
	(7, 7, 1, '16:00:00', NULL, NULL),
	(8, 8, 1, '17:00:00', NULL, NULL),
	(9, 1, 2, '11:00:00', NULL, NULL),
	(10, 2, 2, '12:00:00', NULL, NULL),
	(11, 3, 2, '13:00:00', NULL, NULL),
	(12, 4, 2, '14:00:00', NULL, NULL),
	(13, 5, 2, '15:00:00', NULL, NULL),
	(14, 6, 2, '16:00:00', NULL, NULL),
	(15, 7, 2, '17:00:00', NULL, NULL),
	(16, 8, 2, '18:00:00', NULL, NULL),
	(17, 1, 3, '12:00:00', NULL, NULL),
	(18, 2, 3, '13:00:00', NULL, NULL),
	(19, 3, 3, '14:00:00', NULL, NULL),
	(20, 4, 3, '15:00:00', NULL, NULL),
	(21, 5, 3, '16:00:00', NULL, NULL),
	(22, 6, 3, '17:00:00', NULL, NULL),
	(23, 7, 3, '18:00:00', NULL, NULL),
	(24, 8, 3, '19:00:00', NULL, NULL),
	(25, 1, 4, '13:00:00', NULL, NULL),
	(26, 2, 4, '14:00:00', NULL, NULL),
	(27, 3, 4, '15:00:00', NULL, NULL),
	(28, 4, 4, '16:00:00', NULL, NULL),
	(29, 5, 4, '17:00:00', NULL, NULL),
	(30, 6, 4, '18:00:00', NULL, NULL),
	(31, 7, 4, '19:00:00', NULL, NULL),
	(32, 8, 4, '20:00:00', NULL, NULL),
	(33, 1, 5, '14:00:00', NULL, NULL),
	(34, 2, 5, '15:00:00', NULL, NULL),
	(35, 3, 5, '16:00:00', NULL, NULL),
	(36, 4, 5, '17:00:00', NULL, NULL),
	(37, 5, 5, '18:00:00', NULL, NULL),
	(38, 6, 5, '19:00:00', NULL, NULL),
	(39, 7, 5, '20:00:00', NULL, NULL),
	(40, 8, 5, '21:00:00', NULL, NULL),
	(41, 1, 6, '11:00:00', NULL, NULL),
	(42, 4, 6, '15:00:00', NULL, NULL),
	(43, 6, 6, '21:00:00', NULL, NULL),
	(44, 8, 6, '23:00:00', NULL, NULL),
	(45, 8, 7, '23:00:00', NULL, NULL),
	(46, 6, 7, '21:00:00', NULL, NULL);
/*!40000 ALTER TABLE `bus_locations` ENABLE KEYS */;

-- Dumping structure for table bus_stop_locator.bus_stop_details
CREATE TABLE IF NOT EXISTS `bus_stop_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bus_stop_locator.bus_stop_details: ~7 rows (approximately)
DELETE FROM `bus_stop_details`;
/*!40000 ALTER TABLE `bus_stop_details` DISABLE KEYS */;
INSERT INTO `bus_stop_details` (`id`, `name`, `address`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
	(1, 'Jadhav Corner ( Kala Khadak)', 'Shankar Kalat Nagar, Wakad, Pimpri-Chinchwad, Maharashtra 411057', 18.607, 73.7556, NULL, NULL),
	(2, 'Kala Khadak Vasahat', 'Tathawade, Pimpri-Chinchwad, Maharashtra 411033', 18.607, 73.7554, NULL, NULL),
	(3, 'Pandit Automotive', 'Tathawade, Pimpri-Chinchwad, Maharashtra 411033', 18.6122, 73.7594, NULL, NULL),
	(4, ' State Bank Of India Stop', ' State Bank Of India , Dange Chowk, Thergaon, Pune', 18.6116, 73.7655, NULL, NULL),
	(5, 'Bhumkar Chowk', 'Shankar Kalat Nagar, Wakad, Pimpri-Chinchwad, Maharashtra 411057', 18.6057, 73.7527, NULL, NULL),
	(6, 'Chinchwad', 'Bhumkar Nagar, Wakad, Pimpri-Chinchwad, Maharashtra 411057, India', 18.6043, 73.7547, NULL, NULL),
	(7, 'Pune Ginger Hotel- Wakad ', 'Tathawade, Pimpri-Chinchwad, Maharashtra 411057, India', 18.6049, 73.752, NULL, NULL);
/*!40000 ALTER TABLE `bus_stop_details` ENABLE KEYS */;

-- Dumping structure for table bus_stop_locator.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bus_stop_locator.migrations: ~2 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2018_11_04_123421_create_bus_stop_details_table', 1),
	(2, '2018_11_04_124621_create_bus_locations_table', 1),
	(3, '2018_11_04_124651_create_bus_details_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table bus_stop_locator.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bus_stop_locator.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table bus_stop_locator.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bus_stop_locator.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(2, 'Avinash Kumar', 'avinash@gmail.com', NULL, '$2y$10$GIstjQDmZtYGEb6rPUBlTeEf8A6Tq0RHWDMqnumnlb9VObiL5WAKy', NULL, '2018-11-06 05:40:07', '2018-11-06 05:40:07'),
	(3, 'Avinash Kumar', 'jointoavinash@gmail.com', NULL, '$2y$10$3xO7KiCtuSTBzDcK5qtSBOb1euj38dGD0lDVlnomtAHWgQlZ8jXOS', NULL, '2018-11-06 05:42:49', '2018-11-06 05:42:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
