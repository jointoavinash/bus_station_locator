<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\bus_stop_details;

class BusLocatorController extends Controller
{
    //index
    public function index(Request $request){

        $latitude = $request->lat;
        $lngitude = $request->lng;
        
        $busStop = DB::table('bus_stop_details')->select('id','name','address','lat','lng')
        ->where('lat','<=', ($latitude+0.1))
        ->where('lat','>=', ($latitude-0.1))
        ->where('lng','<=', ($lngitude+0.1))
        ->where('lng','>=', ($lngitude-0.1))
        ->get();        
        //print_r($busStop);
        return $busStop;
    }

    public function getAllBusDetails(Request $request){
        //return $request->id;
        $busTransitDetails = DB::table('bus_stop_details')
        ->join('bus_locations','bus_locations.bus_stop_id', '=' , 'bus_stop_details.id')
        ->join('bus_details','bus_details.id', '=', 'bus_locations.bus_id')
        ->select('bus_details.id', 'bus_stop_details.name as bus_stop', 'bus_details.name', 'bus_locations.arrival_time')
        ->where('bus_stop_details.id', $request->id)
        ->get();   
        return $busTransitDetails;
        //print_r($busTransitDetails);
    }
}
 