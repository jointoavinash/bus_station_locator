@extends('layouts.app')

                                        
<style>
        #map {
            width: 100%;
            height: 600px;
            border: 1px solid black;
        }
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Your Nearest Bus Station</div>

                         <div class="card-body">  
                                    <div id="map">

                                </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=API_KEY&callback=loadMap&libraries=places">
</script>
@endsection
  