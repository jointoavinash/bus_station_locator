var map;
var geocoder;
var infoWind;
var myLatLng;

function loadMap(){
    geoLocationInit();
    //Function to Get user location from browser
    function geoLocationInit() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success, fail);
        } else {
            alert("Browser not supported");
        }
    }

    function success(position) {
        var latval = position.coords.latitude;
        var lngval = position.coords.longitude;

        //Hard coding lat, lang as 18.607060, 73.755620
        //as our database has not enough data to display buses for nearest bus station with dynamic lat,lang
        myLatLng = new google.maps.LatLng(18.607075, 73.757683); //hardcoded lat, lng. 
        createMap(myLatLng);
        searchBusStation(latval,lngval);
    }

    function fail() {
        alert("Something went wrong!");
    }

    //Function to Create Map
    function createMap(myLatLng) {
        console.log(myLatLng);
        map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 15
        });

        //Create marker for user's location
        var marker = new google.maps.Marker({
            position: myLatLng,
            icon:"http://maps.google.com/mapfiles/kml/paddle/grn-blank.png",
            map: map,
            title: 'You are here!'
        });

        //Create circle surrounding to the user location
        var cityCircle = new google.maps.Circle({
            strokeColor: '#bad7f2',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#c6dbef',
            fillOpacity: 0.35,
            map: map,
            center: myLatLng,
            radius: 1200,
        });
    }

    //Create a dynamic Marker for the lat, lng provided
    function createMarker(latLng,points){
        var marker = new google.maps.Marker({
            position: latLng, 
            map: map,
            icon:points.bsicn,
            title: points.bsname
        });

        var content = document.createElement('div');
        var strong = document.createElement('strong');
        
        jQuery(strong).html(getAllBusTiming(points));
        content.appendChild(strong);
        
        infoWind = new google.maps.InfoWindow
        marker.addListener('mouseover',function(){
            infoWind.setContent(content);
            infoWind.open(map, marker);
        });  
    } 
    
    //Function to get arival of all the buses for a bus stop
    function getAllBusTiming(points){
        var getAllBusForDisplay = "<table>";
        getAllBusForDisplay += "<tr><th colspan='3' class='centerText'>"+points.bsName+"</th></tr>";
        getAllBusForDisplay += "<tr><th class='alignLeft'>Srl No</th><th class='alignLeft'>Bus Name</th><th class='alignLeft'>Arrival Time</th></tr>";
        $.ajaxSetup({async: false});
        $.post('http://localhost/bus_stop_locator/public/api/getAllBus',{id:points.id},function(match){        
            //console.log(match);
            $.each(match,function(i,val){
                 console.log(val);
                 getAllBusForDisplay += "<tr><td>"+val.id+"</td><td>"+val.name+"</td><td>"+val.arrival_time+"</td></tr>";
            });
            //var data =jQuery.parseJSON(res);
         });

         getAllBusForDisplay += "</table>";  
         //console.log(getAllBusForDisplay);
         return getAllBusForDisplay;     
    }

    function searchBusStation(lat,lng){
        $.post('http://localhost/bus_stop_locator/public/api/searchBus',{lat:lat,lng:lng},function(match){
            $.each(match,function(i,val){
                var bslatval=val.lat;
                var bslngval=val.lng;
                var points = {};
                points.id =  val.id;
                points.bsName =  val.name;
                points.bsicn= "http://maps.google.com/mapfiles/ms/micons/bus.png";

                var bsLatLng = new google.maps.LatLng(bslatval, bslngval);
                createMarker(bsLatLng,points);
              });
        });
    }

}
//     var myLocation = new google.maps.LatLng(18.607060, 73.755620); //{lat:18.607780, lng:73.764038}
//     var icn = "http://maps.google.com/mapfiles/kml/paddle/grn-blank.png"
//     map = new google.maps.Map(document.getElementById('map'), {
//     center: myLocation,
//     zoom: 16     
//     });

//     var marker = new google.maps.Marker({
//         position: myLocation, 
//         map: map,
//         icon:icn,
//         title: 'Your Are Here!'
//     });

//     var cityCircle = new google.maps.Circle({
//         strokeColor: '#bad7f2',
//         strokeOpacity: 0.8,
//         strokeWeight: 2,
//         fillColor: '#c6dbef',
//         fillOpacity: 0.35,
//         map: map,
//         center: myLocation,
//         radius: 500,
//         icon : "http://labs.google.com/ridefinder/images/mm_20_white.png"
//       });

//     var request = {
//         location: myLocation,
//         radius: '500',
//         type: ['bus_station']
//     };

    
//     service = new google.maps.places.PlacesService(map);
//     service.nearbySearch(request,callback);

//     //Function to make query for busstop
//     function callback(results, status) {
//         console.log(results)
//         if (status == google.maps.places.PlacesServiceStatus.OK) {
//             for (var i = 0; i < results.length; i++) {
//                 var place = results[i];
//                 latLng = place.geometry.location;
//                 icn = place.icon
//                 name = place.name
//                 createMarker(latLng,icn,name);
//             }
//         }
//     }
// }
